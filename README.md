# Gitlab CI iamge with Nix

This repository uses Gitlab CI to build image with Nix. It is suppose to be used
in other Gitlab CI pipelines to support caching in Nix. Scripts provided by this
image track anything you fetch or build and then export those from the
container's Nix store to the cache.


## Usage

You need to use `registry.gitlab.com/cynerd/gitlab-ci-nix` as image in your
pipeline. To actually apply caching you need to call two scripts this image
provides you with. The first one should be called before any other Nix command
and is called `gitlab-ci-nix-cache-before`. The second one should be called
after you issued all Nix commands and is called `gitlab-ci-nix-cache-after`.

The good way to apply this to your pipeline is through a template:

```yml
.nix:
  image: registry.gitlab.com/cynerd/gitlab-ci-nix
  tags:
    - docker
  cache:
    key: "nix"
    paths:
      - ".nix-cache"
  before_script:
    - gitlab-ci-nix-cache-before
  after_script:
    - gitlab-ci-nix-cache-after


build:
    extends: .nix
    script:
        nix build
```

The alternative is to reuse the `.nix` definition from this repository just like
this:

```yml
include:
    - 'https://gitlab.com/Cynerd/gitlab-ci-nix/-/raw/master/template.yml'

build:
    extends: .nix
    script:
        nix build
```


### Gitlab caching

Gitlab provides cache in runners that can be used to share files between runs.
This is default and always expected to be available way of caching. To configure
it you need to just tell Gitlab to cache the `.nix-cache` directory.

The Gitlab cache needs to be in directory relative to the project and thus it is
not possible to tell Gitlab to just cache `/nix`. At the same time it is common
that Gitlab CI Docker runners do not have ability to do mount and thus we can't
use `nix --store`. And thus we export new paths from `/nix/store` to
`.nix-cache`.

### Cachix

Cachix is a common tool used to cache Nix builds. To use it you need to define
variable `CACHIX_NAME` and optionally `CACHIX_KEY`. You should use CI variable
that is masked for the key. Cache is always used as substituter. Fetched and
built paths are pushed to it if you do specify `CACHIX_KEY`.

### SSH

You can use any host with SSH access and Nix as cache. To do so you need to
provide SSH key in variable `NIX_SSH_KEY` and the host URL in `NIX_SSH_URL`.
