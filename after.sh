section_start() {
	local name="$1"
	shift
	echo -e "\e[0Ksection_start:$(date +%s):${name}[collapsed=true]\r\e[0K$*"
}
section_end() {
	local name="$1"
	echo -e "\e[0Ksection_end:$(date +%s):$name\r\e[0K"
}

# Assemble list of new builds
find /nix/store -mindepth 1 -maxdepth 1 ! -name \*.drv |
	sort >/nix/.after
comm -13 /nix/.before /nix/.after >/nix/.new

[ -s /nix/.new ] ||
	exit 0

# Export new builds to Gitlab CI Runner cache
section_start gitlab_ci_cache Copy to Gitlab CI cache
xargs -a /nix/.new nix copy --to "file://$(pwd)/.nix-cache"
section_end gitlab_ci_cache

# Export to Cachix
if [ -n "${CACHIX_NAME:-}" ] && [ -n "${CACHIX_KEY:-}" ]; then
	section_start cachix Copy to Cachix
	cachix authtoken "$CACHIX_KEY"
	cachix push "$CACHIX_NAME" </nix/.new
	section_end cachix
fi

# Export to SSH cache
if [ -n "${NIX_SSH_URL:-}" ] && [ -n "${NIX_SSH_KEY:-}" ]; then
	section_start ssh Copy to SSH
	xargs -a /nix/.new nix copy --to "ssh://$NIX_SSH_URL"
	section_end ssh
fi
