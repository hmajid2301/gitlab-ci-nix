{
  description = "Docker image for Gitlab CI";

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    nix,
  }: let
    inherit (builtins) readFile;
    inherit (flake-utils.lib) eachDefaultSystem;
  in
    eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages."${system}";
        spkgs = self.packages."${system}";
      in {
        packages = {
          cache-before-script = pkgs.writeShellApplication {
            name = "gitlab-ci-nix-cache-before";
            runtimeInputs = [pkgs.nix];
            text = readFile ./before.sh;
          };
          cache-after-script = pkgs.writeShellApplication {
            name = "gitlab-ci-nix-cache-after";
            runtimeInputs = [pkgs.nix];
            text = readFile ./after.sh;
          };
          gitlab-ci-nix = import (nix.outPath + "/docker.nix") {
            inherit pkgs;
            name = "gitlab-ci-nix";
            bundleNixpkgs = false;
            extraPkgs = [
              spkgs.cache-before-script
              spkgs.cache-after-script
              pkgs.cachix
            ];
            nixConf = {
              cores = "0";
              system-features = ["nixos-test" "benchmark" "big-parallel" "kvm"];
              experimental-features = ["nix-command" "flakes" "auto-allocate-uids"];
              accept-flake-config = "true";
              sandbox = "true";
              sandbox-fallback = "true";
              auto-allocate-uids = "true";
            };
          };
          default = spkgs.gitlab-ci-nix;
        };

        formatter = pkgs.alejandra;
      }
    );
}
