cachedir="$(pwd)/.nix-cache"
signkey="$cachedir/signkey"
mkdir -p "$cachedir"

# Generate sign key if not already present
[ -f "$signkey" ] ||
	nix key generate-secret --key-name local >"$signkey"

# Nix configuration
{
	echo "extra-secret-key-files = $signkey"
	echo "extra-substituters = file://$cachedir?priority=10"
	echo "extra-trusted-public-keys = $(
		nix key convert-secret-to-public <"$signkey"
	)"
} >>/etc/nix/nix.conf

# Enable cachix if provided by environment
if [ -n "${CACHIX_NAME:-}" ]; then
	cachix use "$CACHIX_NAME"
fi

# Enable SSH substituter
if [ -n "${NIX_SSH_URL:-}" ] && [ -n "${NIX_SSH_KEY:-}" ]; then
	ssh-add - <<<"$NIX_SSH_KEY"
	echo "extra-substituters = ssh://$NIX_SSH_URL" >>/etc/nix/nix.conf
fi

# Save the current store content
find /nix/store -mindepth 1 -maxdepth 1 ! -name \*.drv |
	sort >/nix/.before
